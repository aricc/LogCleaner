﻿# LogCleaner
一个日志清理工具。可按指定的目录列表，指定的时间长度，指定的文件类型检查日志目录中的文件是否已经过期，并将过期文件删除。


配置示例：
```
<?xml version="1.0" encoding="utf-8" ?>
<Dirs>
  <!--
  Path：要监控的目录
  Filter：要清除的文件
  TimeLen：删除的时间点（单位：y年M月d天H小时m分钟）。区分大小写。如果配置的单位不在这5个中，则默认为d。不支持秒级别。
  SubDir：是否检查子目录（递归）。默认不检查。
  DeleteEmptySubDir：如果子目录为空时是否自动删除。默认不删除。
  如果目录配置重复，以最后一行配置为准。
  文件修改后重启生效。
  -->
  <Dir Path="D:\DSSystem\DSECS\Logs" Filter="*.log,*.txt" TimeLen="7d" SubDir="true" DeleteEmptySubDir="true" />
  <Dir Path="D:\DSSystem\PoliceTerminalService\Logs" Filter="*.*" TimeLen="1m" />
</Dirs>
```

