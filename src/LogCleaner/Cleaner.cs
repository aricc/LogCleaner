using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace BingeSoft.LogCleaner
{
    /// <summary>
    /// 清理器
    /// 只监控一个目录
    /// </summary>
    public class Cleaner
    {
        log4net.ILog log = log4net.LogManager.GetLogger(typeof(Cleaner));

        #region 配置
        public string Path { get; set; } = null;
        public List<string> Filters = new List<string>();
        public string TimeLen = null;
        public bool SubDir = false;
        public bool DeleteEmptySubDir = false;
        #endregion

        //监控定时器
        private System.Timers.Timer timer = null;

        public void Start()
        {
            timer = new System.Timers.Timer();
            timer.Interval = 1000 * 60;//最小粒度1分钟
            timer.Elapsed += new System.Timers.ElapsedEventHandler(timer_Elapsed);
            timer.Start();

            log.Info("清理器监控已开启（" + this.Path + "）");
        }

        void timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            timer.Stop();
            try
            {
                DateTime checkPoint = this.GetCheckPoint(TimeLen);
                this.CleanDir(Path, checkPoint);
            }
            catch (Exception ex)
            {
                log.Error("清理日志时异常：(" + this.Path + ")" + ex.Message + "\r\n" + ex.StackTrace);
            }
            timer.Start();
        }

        private void CleanDir(string path, DateTime checkPoint, bool deleteIfEmpty = false)
        {
            log.Info("开始清理日志（清理最后修改日期早于" + checkPoint.ToString("yyyy-MM-dd HH:mm:ss") + "的文件于目录" + path + "）");

            DirectoryInfo dir = new DirectoryInfo(path);
            if (dir == null) return;

            //文件清理
            foreach (string filter in Filters)
            {
                FileInfo[] files = dir.GetFiles(filter);
                if (files != null)
                {
                    foreach (FileInfo file in files)
                    {
                        if (file.LastWriteTime < checkPoint)
                        {
                            try
                            {
                                file.Delete();
                                log.Info("文件已删除" + file.FullName);
                            }
                            catch (Exception ex)
                            {
                                log.Info(file.FullName + "删除失败：" + ex.Message + "\r\n" + ex.StackTrace);
                            }
                        }
                    }
                }
            }
            if (SubDir)
            {
                DirectoryInfo[] dirs = dir.GetDirectories();
                if (dirs != null)
                {
                    foreach (DirectoryInfo subDir in dirs)
                    {
                        CleanDir(subDir.FullName, checkPoint, DeleteEmptySubDir);
                    }
                }
            }
            if (deleteIfEmpty)//如果配置为本目录为空时自动删除本目录
            {
                FileInfo[] files = dir.GetFiles();
                DirectoryInfo[] dirs = dir.GetDirectories();
                if(files.Length==0 && dirs.Length == 0)//如果文件和子文件夹都没有了。
                {
                    log.Info("子目录已删除" + dir.FullName);
                    dir.Delete();
                }
            }
        }

        private DateTime GetCheckPoint(string cfg)
        {
            cfg = cfg.Trim();
            if (cfg.Length < 2)
            {
                cfg = "7d";
            }
            string dw = cfg.Substring(cfg.Length - 1);
            int cv = 7;
            if (!int.TryParse(cfg.Substring(0, cfg.Length - 1), out cv)) cv = 7;

            DateTime checkPoint;
            switch (dw)
            {
                case "y":
                    checkPoint = DateTime.Now.AddYears(-1 * cv);
                    break;
                case "M":
                    checkPoint = DateTime.Now.AddMonths(-1 * cv);
                    break;
                case "H":
                    checkPoint = DateTime.Now.AddHours(-1 * cv);
                    break;
                case "m":
                    checkPoint = DateTime.Now.AddMinutes(-1 * cv);
                    break;
                case "d":
                default:
                    checkPoint = DateTime.Now.AddDays(-1 * cv);
                    break;
            }

            return checkPoint;
        }

        public void Stop()
        {
            timer.Stop();
        }
    }
}
