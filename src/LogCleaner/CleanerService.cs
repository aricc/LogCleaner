﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;

namespace BingeSoft.LogCleaner
{
    partial class CleanerService : ServiceBase
    {
        log4net.ILog log = log4net.LogManager.GetLogger(typeof(CleanerService));

        public CleanerService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            log.Debug("服务正在启动...");
            Manager.Start();
            log.Debug("服务已启动");
        }

        protected override void OnStop()
        {
            log.Debug("服务正在停止...");
            Manager.Stop();
            log.Debug("服务已停止");
        }
    }
}
