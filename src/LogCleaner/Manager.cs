using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace BingeSoft.LogCleaner
{
    /// <summary>
    /// 清理器管理器
    /// </summary>
    public static class Manager
    {
        private static log4net.ILog log = log4net.LogManager.GetLogger(typeof(Manager));

        //清理器集合
        private static Dictionary<string, Cleaner> Cleaners = null;

        public static void Start()
        {
            //System.Diagnostics.Debugger.Launch();

            Cleaners = new Dictionary<string, Cleaner>();
            LoadConfig();
            foreach (Cleaner cc in Cleaners.Values)
            {
                cc.Start();
            }
        }
        public static void Stop()
        {
            foreach (Cleaner cc in Cleaners.Values)
            {
                cc.Stop();
            }
        }

        /// <summary>
        /// 加载配置
        /// </summary>
        private static void LoadConfig()
        {
            try
            {
                log.Debug("正在加载配置...");
                XmlDocument xml = new XmlDocument();
                xml.Load(System.AppDomain.CurrentDomain.BaseDirectory + "Config.xml");

                XmlNode rootNode = xml.SelectSingleNode("Dirs");
                if (rootNode != null)
                {
                    XmlNodeList dirList = rootNode.SelectNodes("Dir");
                    if (dirList != null)
                    {
                        foreach (XmlNode node in dirList)
                        {
                            Cleaner cc = new Cleaner();
                            cc.Path = node.Attributes["Path"].Value;
                            string[] filters;
                            try
                            {
                                filters = node.Attributes["Filter"].Value.Split(new string[] { ",", "，" }, StringSplitOptions.RemoveEmptyEntries);
                            }
                            catch { filters = new string[] { "*.*" }; }
                            foreach (string item in filters)
                            {
                                cc.Filters.Add(item);
                            }
                            try
                            {
                                cc.TimeLen = node.Attributes["TimeLen"].Value;
                            }
                            catch { cc.TimeLen = "7d"; }
                            try
                            {
                                cc.SubDir = node.Attributes["SubDir"].Value.ToUpper() == "TRUE";
                            }
                            catch { cc.SubDir = false; }
                            try
                            {
                                cc.DeleteEmptySubDir = node.Attributes["DeleteEmptySubDir"].Value.ToUpper() == "TRUE";
                            }
                            catch { cc.DeleteEmptySubDir = false; }

                            //同目录配置，覆盖更新
                            if (Cleaners.ContainsKey(cc.Path))
                            {
                                Cleaners[cc.Path] = cc;
                            }
                            else
                            {
                                Cleaners.Add(cc.Path, cc);
                            }
                        }
                    }
                }
                log.Debug("配置已加载");
            }
            catch (Exception ex)
            {
                log.Error("加载配置时异常：" + ex.Message + "\r\n" + ex.StackTrace);
            }
        }
    }
}
